<?php
# ex: tabstop=8 shiftwidth=8 noexpandtab
/**
* @package MediaWiki
* @subpackage jQuery
* @author Jim Tittsler <jim@OERfoundation.org>
* @licence GPL2
*/

if( !defined( 'MEDIAWIKI' ) ) {
	die( "This file is an extension to the MediaWiki software and cannot be used standalone.\n" );
}

$wgExtensionCredits['parserhook'][] = array(
	'name'           => 'jQuery',
	'version'        => '1.1',
	'url'            => 'http://WikiEducator.org/Extension:jQuery',
	'author'         => '[http://WikiEducator.org/User:JimTittsler Jim Tittsler]',
        'description'    => 'inject jQuery in older MediaWiki',
);

$wgHooks['BeforePageDisplay'][] = 'wejQuery';

$wgjQuerysrc = "http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js";
function wejQuery( &$out, &$sk ) {
	global $wgjQuerysrc;
	// avoid adding jQuery if it looks like another extension
	//   (or widget?) appears to have already added it
	if ( preg_match('/\/?jquery[-.0-9min]*\.js/i', $out->mScripts) == 0 ) {
		// put jQuery first of the extension added JavaScript
		$out->mScripts = '<script type="text/javascript" src="' .
			$wgjQuerysrc . '"></script>' . "\n" . $out->mScripts;
	}
	return true;
}
 
