# README #

This obsolete Mediawiki extension added [jQuery](https://jquery.org/) to every page (before jQuery support was built into Mediawiki).

### License ###

GPLv2